<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NodeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => $this->faker->uuid(),
            'node_name' => $this->faker->name(),
            'department' => $this->faker->word(),
            'height' => 1
        ];
    }
}
