<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Models\Node;

class NodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('nodes')->insert([
            'uuid' => Str::uuid(),
            'node_name' => 'Root',
            'height' => 0,
            'created_at' => now(),
        ]);
        
        Node::factory(5)->create();
    }
}
