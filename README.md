## StepFront API

### Prerequisites
Following tools should be installed beforehand:
1. PHP version >= 7.3
2. MySQL 5 or above
3. Composer package manager for PHP
4. Git

### Requirements
1. Basic knowledge of Git
2. Basic knowledge of command line

### Installation
1. Clone the repository or download the package to your local folder.
2. Create .env file from .env.example.
3. Create a database and set your database settings (database name, server name, username and password) in your .env file.
4. Open a terminal/command prompt and browse to the project root folder.
5. Run `composer install` to install all dependencies.
6. Run `php artisan migrate` to create tables and update data from the command line in your project root folder.
7. Run `php artisan db:seed --class=NodeSeeder` to seed the database.
8. To run the application locally, run `php artisan serve` from your project root folder.
9. To run tests, run `vendor/bin/phpunit tests/Feature/NodeTest.php` from the terminal.

Once served with artisan, the api will be available at http://127.0.0.1:8000/api. 

### API Endpoints
1. /nodes/add
```
    method: POST
    data type: JSON
    data format : {
        node_name: string,
        parent_id: integer,
        department: string (optional),
        programming_language: string (optional)
    }
    return type: JSON
    sample output: {
        "data": {
            "node_name": "test new node",
            "parent_id": 2,
            "department": "new department",
            "height": 2,
            "uuid": "eda3dc23-6469-48c3-9489-1ef29aed9cd5",
            "updated_at": "2022-03-02T10:50:58.000000Z",
            "created_at": "2022-03-02T10:50:58.000000Z",
            "id": 21
        }
    }
```
2. /nodes/node-children/{id}
```
    method: GET
    parameter: id, integer, id of the node,
    return type: JSON,
    sample output: {
        "data": [
            {
            "id": 4,
            "uuid": "b269b86a-a514-3623-818b-2b14f95a911d",
            "node_name": "Maritza Bergstrom",
            "parent_id": "2",
            "height": 1,
            "department": "architecto",
            "programming_language": null,
            "created_at": "2022-03-02T09:28:55.000000Z",
            "updated_at": "2022-03-02T10:30:01.000000Z"
            },
            {
            "id": 10,
            "uuid": "0c2dfd42-cb49-44c4-aa84-30bd1952ba55",
            "node_name": "testName5",
            "parent_id": "2",
            "height": 2,
            "department": null,
            "programming_language": null,
            "created_at": "2022-03-02T09:55:51.000000Z",
            "updated_at": "2022-03-02T09:55:51.000000Z"
            },
            {
            "id": 20,
            "uuid": "29dc5771-6101-4fc6-9142-4489d252370b",
            "node_name": "new child",
            "parent_id": "2",
            "height": 2,
            "department": null,
            "programming_language": null,
            "created_at": "2022-03-02T10:06:07.000000Z",
            "updated_at": "2022-03-02T10:06:07.000000Z"
            },
            {
            "id": 21,
            "uuid": "eda3dc23-6469-48c3-9489-1ef29aed9cd5",
            "node_name": "test new node",
            "parent_id": "2",
            "height": 2,
            "department": "new department",
            "programming_language": null,
            "created_at": "2022-03-02T10:50:58.000000Z",
            "updated_at": "2022-03-02T10:50:58.000000Z"
            }
        ]
    }
```
3. /nodes/change-parent/{id}
```
    method: PUT
    parameter: id, integer, id of the node,
    data type: JSON,
    data format: {
        parent_id: int, id of new parent node
    }
    return type: JSON
    sample output: {
        "data": {
            "node_name": "test new node",
            "parent_id": 2,
            "department": "new department",
            "height": 2,
            "uuid": "eda3dc23-6469-48c3-9489-1ef29aed9cd5",
            "updated_at": "2022-03-02T10:50:58.000000Z",
            "created_at": "2022-03-02T10:50:58.000000Z",
            "id": 21
        }
    }
```

### Description:
If I had some more time to work on this, I would have done the following updates:

1. Create resource and resource collection and use them to response
2. Create OpenAPI documentation 