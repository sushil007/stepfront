<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/nodes')->group(function(Router $route) {
    Route::post('/add', [App\Http\Controllers\Api\NodeController::class, 'add'])->name('addNode');
    Route::get('/node-children/{id}', [App\Http\Controllers\Api\NodeController::class, 'children'])->name('nodeChildren');

    Route::put('/change-parent/{id}', [App\Http\Controllers\Api\NodeController::class, 'changeParent'])->name('changeParent');
});