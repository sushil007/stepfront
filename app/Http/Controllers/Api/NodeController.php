<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Services\NodeService;
use App\Models\Node;

class NodeController extends Controller
{
    /**
     * @var NodeService
     */
    public NodeService $service;

    public function __construct(NodeService $service)
    {
        $this->service = $service;
    }

    //
    /**
     * @param Request $request
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'node_name' => 'required|string',
            'parent_id' => 'integer',
            'department'    =>  'string|nullable',
            'programming_language'  =>  'string|nullable'
            ]
        );

        if($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $node = $this->service->addNode($validator->validated());
        return response()->json(['data'=>$node], 200);
    }

    /**
     * @param int $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function children(int $id)
    {
        $data = $this->service->getChildren($id);
        return response()->json(['data'=>$data], 200);
    }

    /**
     * @param int     $id
     * @param Request $request
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeParent(int $id, Request $request)
    {
        if(!($node = Node::find($id))) {
            return response()->json('Node not found', 404);
        }
        
        $validator = Validator::make(
            $request->all(), [
            'parent_id' => [
                    'integer',
                    Rule::in(Node::pluck('id')->all())
                ],
            ]
        );

        if($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $node = $this->service->updateParent($node, $validator->validated());

        return response()->json(['data'=>$node], 200);
    }
}
