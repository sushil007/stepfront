<?php
namespace App\Services;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Collection;

use App\Models\Node;

class NodeService
{

    public function addNode(array $data) : Node
    {
        $height = $this->getParentDepth($data['parent_id'])+1;
        $data = array_merge($data, ['height'=>$height, 'uuid'=>Str::uuid()]);
        return Node::create($data);
    }

    protected function getParentDepth($parentID) : int
    {
        $parent = Node::find($parentID);
        return $parent->height;    
    }

    public function getChildren(int $id) : Collection
    {
        return Node::where('parent_id', '=', $id)->get();
    }

    public function updateParent(Node $node, array $data) : Node
    {
        $node->parent_id = $data['parent_id'];
        $node->save();
        return $node;
    }
}