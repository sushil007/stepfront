<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
    use HasFactory;

    protected $fillable = ['uuid', 'node_name', 'parent_id', 'height', 'department', 'programming_language', 'created_at', 'updated_at'];
}
