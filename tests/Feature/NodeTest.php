<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Testing\Fluent\AssertableJson;

class NodeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_add_new_node()
    {
        $nodeData = [
            'node_name' => 'test Name5',
            'parent_id' => 9
        ];
        $response = $this->postJson('/api/nodes/add', $nodeData);        

        $response->assertStatus(200)
                ->assertJson(fn(AssertableJson $json) =>
                    $json->has('data')
                    ->has('data.uuid')        
                );

        $nodeData = [
            'node_name' => 'new child',
            'parent_id' => 2
        ];
        $response = $this->postJson('/api/nodes/add', $nodeData);        

        $response->assertStatus(200)
                ->assertJson(fn(AssertableJson $json) =>
                    $json->has('data')
                    ->has('data.uuid')        
                );

    }

    public function test_get_child_nodes() {
        $nodeID = rand(1, 9);
        $response = $this->getJson('api/nodes/node-children/'.$nodeID);
        $response->assertStatus(200)
            ->assertJson(fn(AssertableJson $json) =>
                $json->has('data')
        );

    }

    public function test_change_parent() {
        $nodeData = [
            'parent_id' => 3
        ];

        $nodeID = rand(7, 18);
        $response = $this->putJson('api/nodes/change-parent/'.$nodeID, $nodeData);

        $response->assertStatus(200)
                    ->assertJson(fn(AssertableJson $json) => 
                        $json->has('data')
                            ->has('data.uuid')
            );
    }
}
